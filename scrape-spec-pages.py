#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import sys
from re import search
import requests
from bs4 import BeautifulSoup
import car_models


def inch_to_mm(inch):
    return float(inch * 25.4)


def gal_to_l(gal):
    return float(gal * 3.785412)


def lbs_to_kg(lbs):
    return int(lbs * 0.45359237)


def main(json_file):
    """Entry point."""
    try:
        url_file = open('newUrls.txt')
        urls = url_file.readlines()
        url_file.close()
        for idx, line in enumerate(urls, 1):
            print("{}:{}".format(idx, line.strip()))
            scrape(line.strip(), json_file)
    except KeyboardInterrupt:
        print("CTRL+C - Exiting...")
    finally:
        if not url_file.closed:
            url_file.close()


def scrape(url, json_file):
    """Scrape a given caranddriver.com URL and parse data."""
    req = requests.get(url)
    soup = BeautifulSoup(req.text, 'html.parser')
    find_try = "= ("
    try:
        text = soup.select("html > body > universal-script > script")[0].contents[0].strip()
    except IndexError:
        print("Index error on url: {}".format(url))
        return
    text = text[text.find(find_try)+len(find_try):]
    find_catch = text.find(") || {};")
    text = text[:find_catch]
    parsed = json.loads(text)
    parsed_cache = json.loads(parsed["Cache"])
    for k, v in parsed_cache.items():
        if len(v) < 1:
            continue
        if isinstance(v, list) and isinstance(v[0], dict):
            if 'dataset' in v[0]:
                styleData = v[0]['dataset']['configuration']['style']
                techSpecs = v[0]['dataset']['configuration']["technicalSpecifications"]
                standardEquipment = v[0]['dataset']['configuration']["standardEquipment"]
                c = car_models.Vehicle()
                c.year = styleData['modelYear']
                c.make.name = styleData['divisionName']
                c.model.name = styleData["consumerFriendlyStyleName"]
                c.trim.name = styleData['trimName']
                c.configuration.drivetrain.drive = styleData["consumerFriendlyDrivetrain"]
                c.configuration.name = styleData["consumerFriendlyStyleName"]
                c.configuration.msrp = styleData["baseMsrp"]
                c.configuration.number_of_doors = styleData["passengerDoors"]
                c.configuration.body_style = styleData["consumerFriendlyBodyType"]
                c.configuration.classification.name = styleData["marketClassName"]
                for it in standardEquipment:
                    if "Transmission:" in it["description"]:
                        c.configuration.drivetrain.transmission.code = it["description"].split(':')[1].strip()
                for it in techSpecs:
                    if it["titleName"] == "Passenger Volume":
                        if not len(it["value"]):
                            continue
                        if it["value"] != "N/A":
                            try:
                                c.configuration.passenger_vol_cu_in = float(it["value"])
                            except ValueError:
                                c.configuration.passenger_vol_cu_in = float(it["value"].split()[0])
                    elif it["titleName"] == "EPA Fuel Economy Est - Hwy":
                        regex = search(r"(\d+)", it["value"])
                        if regex is not None:
                            mpg = int(regex.group(1))
                            c.configuration.mpg_highway = mpg
                        else:
                            c.configuration.mpg_highway = 0.0
                    elif it["titleName"] == "EPA Fuel Economy Est - City":
                        regex = search(r"(\d+)", it["value"])
                        if regex is not None:
                            mpg = int(regex.group(1))
                            c.configuration.mpg_city = mpg
                        else:
                            c.configuration.mpg_city = 0.0
                    elif it["titleName"] == "Fuel Economy Est-Combined":
                        if it["value"] != "N/A":
                            try:
                                c.configuration.mpg_combined = float(it["value"])
                            except ValueError:
                                c.configuration.mpg_combined = (c.configuration.mpg_city + c.configuration.mpg_highway) / 2.0
                        else:
                            c.configuration.mpg_combined = (c.configuration.mpg_city + c.configuration.mpg_highway) / 2.0
                    elif it["titleName"] == "Passenger Capacity":
                        c.configuration.seats = int(it["value"])
                    elif it["titleName"] == "Passenger Volume":
                        try:
                            c.configuration.passenger_vol_cu_in = int(it["value"])
                        except ValueError:
                            c.configuration.passenger_vol_cu_in = None
                    elif it["titleName"] == "Base Curb Weight":
                        if it["value"] != "N/A":
                            #print(">>>{}<<<".format(it["value"]))
                            c.configuration.curb_weight_lb = int(''.join(it["value"].split('-')[0].split(',')[0].split()[0]))
                            c.configuration.curb_weight_kg = lbs_to_kg(c.configuration.curb_weight_lb)
                    elif it["titleName"] == "Length, Overall":
                        try:
                            c.configuration.length_in = float(it["value"])
                            c.configuration.length_mm = inch_to_mm(c.configuration.length_in)
                        except ValueError:
                            pass
                    elif it["titleName"] == "Width, Max w/o mirrors":
                        try:
                            c.configuration.width_in = float(it["value"])
                            c.configuration.width_mm = inch_to_mm(c.configuration.width_in)
                        except ValueError:
                            pass
                    elif it["titleName"] == "Height, Overall":
                        try:
                            c.configuration.height_in = float(it["value"])
                            c.configuration.height_mm = inch_to_mm(c.configuration.height_in)
                        except ValueError:
                            pass
                    elif it["titleName"] == "Wheelbase":
                        try:
                            c.configuration.wheelbase_in = float(it["value"])
                            c.configuration.wheelbase_mm = inch_to_mm(c.configuration.wheelbase_in)
                        except ValueError:
                            pass
                    elif it["titleName"] == "Fuel Tank Capacity, Approx":
                        if "value" in it:
                            try:
                                c.configuration.fuel_capacity_g = float(it["value"])
                                c.configuration.fuel_capacity_l = gal_to_l(c.configuration.fuel_capacity_g)
                            except ValueError:
                                pass
                    elif it["titleName"] == "Engine Type":
                        engine_type = it["value"].lower()
                        c.configuration.drivetrain.engine.engine_type = engine_type
                        if "twin turbo" in engine_type:
                            c.configuration.drivetrain.engine.forced_induction = "Twin Turbo"
                        elif "turbo" in engine_type:
                            c.configuration.drivetrain.engine.forced_induction = "Turbo"
                        elif "biturbo" in engine_type or  "bi-turbo" in engine_type:
                            c.configuration.drivetrain.engine.forced_induction = "BiTurbo"
                        elif "supercharge" in engine_type:
                            c.configuration.drivetrain.engine.forced_induction = "Supercharged"
                        if "hybrid" in engine_type:
                            c.configuration.drivetrain.engine.engine_type = "Hybrid"
                        elif "elec" in engine_type:
                            c.configuration.drivetrain.engine.engine_type = "Electric"
                        else:
                            c.configuration.drivetrain.engine.engine_type = "Internal Combustion"
                        match = search(r"(V|V-|I|W|H|Flat|Gas)\s*(\d+)", engine_type)
                        if not match:
                            # likely an electric car
                            c.configuration.drivetrain.engine.cylinders = 0
                        else:
                            c.configuration.drivetrain.engine.cylinders = match.group(2)
                            conf = match.group(1).split('-')[0]
                            c.configuration.drivetrain.engine.config = "Flat" if (conf == "Gas") else conf
                    elif it["titleName"] == "Displacement":
                        regex = search(r"(\d\.\d)", it["value"])
                        if regex is not None:
                            liters = regex.group(1)
                            c.configuration.drivetrain.engine.displacement_l = float(liters)
                            try:
                                #print("||DISP||{}||/DISP||".format(it["value"].lower()))
                                if "-tbd-" in it["value"].lower():
                                    # TODO convert myself
                                    continue
                                c.configuration.drivetrain.engine.displacement_cu_in = float(it["value"].split('/')[1])
                                c.configuration.drivetrain.engine.displacement_cc = c.configuration.drivetrain.engine.displacement_cu_in / 0.061024
                            except IndexError:
                                pass # TODO convert myself
                    elif it["titleName"] == "SAE Net Horsepower @ RPM":
                        hp = it["value"].split('@')[0]
                        try:
                            rpm = it["value"].split('@')[1]
                        except IndexError:
                            rpm = 0
                            continue
                        try:
                            c.configuration.drivetrain.engine.max_power_hp = int(hp)
                            #print("|RPM|{}|/RPM|".format(rpm))
                            c.configuration.drivetrain.engine.max_power_rpm = int(rpm.split('-')[0])
                        except ValueError:
                            pass
                    elif it["titleName"] == "SAE Net Torque @ RPM":
                        nt = it["value"].split('@')[0]
                        try:
                            rpm = it["value"].split('@')[1]
                        except IndexError:
                            rpm = 0
                            continue
                        try:
                            c.configuration.drivetrain.engine.max_torque_lb_ft = int(nt)
                            c.configuration.drivetrain.engine.max_torque_rpm = int(rpm.split('-')[0])
                        except ValueError:
                            pass
                    elif it["titleName"] == "Fuel System" and len(it["value"]) > 4:
                        c.configuration.drivetrain.engine.fuel_type = it["value"].split()[0]
                        c.configuration.drivetrain.engine.fuel_delivery = ' '.join(it["value"].split()[1:])
                    elif it["titleName"] == "Trans Type":
                        try:
                            c.configuration.drivetrain.transmission.number_of_gears = int(it["value"])
                        except ValueError:
                            pass
                    elif it["titleName"] == "Trans Description Cont.":
                        c.configuration.drivetrain.transmission.transmission_type = it["value"]
                with open(json_file, 'a') as f:
                    f.write(c.toJSON() + ',\n')


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        main("data.json")
