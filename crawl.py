#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import re
import requests
from queue import Queue
from threading import Event
from threading import Thread
from threading import Lock
from bs4 import BeautifulSoup
from urllib.parse import urljoin

BANNER = "Web Crawler"
NUM_THREADS = 8
ROOT_URL = "http://www.caranddriver.com/"


class CarAndDriverCrawl:
    """A crawler for caranddriver.com."""

    def __init__(self, root):
        self.root = root
        self.visited = set(root)  # protect w/ lock
        self.visitedLock = Lock()
        self.keepers = []  # protect w/ lock
        self.keepersLock = Lock()

    def parse(self, request):
        """Parse request and extract links."""
        if request is not None:
            soup = BeautifulSoup(request.text, 'html.parser')
        if soup is None:
            return []
        urls = []
        for tag in soup.findAll('a', href=True):
            tag['href'] = urljoin(self.root, tag['href'])
            self.visitedLock.acquire()
            if tag['href'][:len(self.root)] == self.root and tag['href'] not in self.visited:
                urls.append(tag['href'])
                self.visited.add(tag['href'])
                if re.search('/specs', tag['href']):
                    self.keepersLock.acquire()
                    self.keepers.append(tag['href'])
                    self.keepersLock.release()
                    with open("spec_urls.txt", "a") as f:
                        f.write(tag['href'] + '\n')
            self.visitedLock.release()
        return urls

    def scrape(self, url):
        """Run scrape on given root url."""
        try:
            request = requests.get(url)
        except requests.exceptions.RequestException as e:
            return None
        return self.parse(request)

    def handle_data(self, worker_id, data):
        urls = self.scrape(data)
        self.keepersLock.acquire()
        print("[handle_data: %d] url: %s | keepers: %d | urls: %d" % (worker_id, data, len(self.keepers), len(urls) if urls is not None else 0))
        self.keepersLock.release()
        if urls is not None:
            for url in urls:
                self.queue.put(url, block=True)

    def process_queue(self, worker_id):
        """Continuously process tasks on the queue."""
        while not self.stop_event.is_set():
            try:
                size = self.queue.qsize()
                print("[%d] Processing queue of size %d" % (worker_id, size))
                data = self.queue.get(block=True)
                self.handle_data(worker_id, data)
                # self.queue.task_done()
            except Exception as e:
                print("Exception in process_queue [%d].\n\t%s" % (worker_id, e))
        print("Stopped worker thread: %d" % worker_id)

    def run(self):
        # get initial list of urls from root
        urls = self.scrape(self.root)
        print("keepers: %d | urls: %d" % (len(self.keepers), len(urls)))
        self.queue = Queue()
        for url in urls:
            self.queue.put(url)
        self.stop_event = Event()
        self.workers = []
        for worker_id in range(NUM_THREADS):
            worker = Thread(target=self.process_queue, args=[worker_id])
            worker.daemon = True
            self.workers.append(worker)
            worker.start()

        [t.join() for t in self.workers]


def main():
    print("\n%s\n" % BANNER)
    try:
        cad = CarAndDriverCrawl(ROOT_URL)
        cad.run()
    except KeyboardInterrupt:
        sys.stdout.write("\n\033[04;31mError\033[00;49m: Interrupted by user\n")


if __name__ == "__main__":
    main()
