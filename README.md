# Car data web scraper

Installing dependencies:
```bash
pip install -r reqs.txt
```

To run the link scrape:
```
python crawl.py
```

To run the crawl, type:

```
scrapy crawl cad -o cars.json -t json
```

This will create a cars.json which has all car data in it.

To populate and make a sqlite3 db, run:

```
python db_populate.py
```

The database will be named cars.db.

See all the cars:
```
sqlite3 cars.db
sqlite> select year, cars_make.name, cars_model.name, trim from cars_car, cars_make, cars_model where cars_car.make_id=cars_make.id and cars_car.model_id=cars_model.id;
```

