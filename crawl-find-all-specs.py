#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import re
import requests
import json
from bs4 import BeautifulSoup
from urllib.parse import urljoin
import socket


def main():
    with open('spec_urls.txt') as f:
        for line in f:
            print("{}".format(line.strip()))
            fetch(line.strip())


def fetch(url):
    print("Fetching... {}".format(url))
    req = requests.get(url)
    soup = BeautifulSoup(req.text, 'html.parser')
    find_try = "= ("
    try:
        text = soup.select("html > body > universal-script > script")[0].contents[0].strip()
    except IndexError:
        print("Index error on url: {}".format(url))
        return
    text = text[text.find(find_try)+len(find_try):]
    find_catch = text.find(") || {};")
    text = text[:find_catch]
    parsed = json.loads(text)
    parsed_cache = json.loads(parsed["Cache"])
    submodel_group = None
    for group in list(parsed_cache.values()):
        try:
            #print(group)
            if 'submodel_group' in group[0] and isinstance(group[0]['submodel_group'], str):
                submodel_group = group[0]['submodel_group']
                break
        except KeyError:
            print('Oops KeyError!')
    for x in list(parsed_cache.values()):
        for y in x:
            with open('test.txt', 'a') as f:
                f.write('{}'.format(y))
                f.write('\n------------\n')
            if isinstance(y, list):
                models_years = y
                break
    if submodel_group is None: return
    for i in models_years:
        spec = "{}/{}/{}".format(i['dataset']['configuration']['style']['modelYear'], submodel_group, i['chrome_id'])
        print(spec)
        with open('newUrls.txt', 'a') as f:
            f.write(url + '/' + spec + '\n')


if __name__ == "__main__":
    main()

