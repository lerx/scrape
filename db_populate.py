#!/usr/bin/python

import sqlite3 as lite
import sys
import json

def country_image_filename(country):
    if country == "USA":
        return "us.gif"
    elif country == "France":
        return "fr.gif"
    elif country == "Germany":
        return "de.gif"
    elif country == "Italy":
        return "it.gif"
    elif country == "Japan":
        return "jp.gif"
    elif country == "Netherlands":
        return "nl.gif"
    elif country == "South Korea":
        return "kr.gif"
    elif country == "Sweden":
        return "se.gif"
    elif country == "UK":
        return "gb.gif"
    else:
        # default to USA
        return "us.gif"

def db_populate():
    json_data = []

    with open('cars.json') as f:
        json_data = json.loads(f.read())

    con = lite.connect('cars.db')

    with con:
    
        cur = con.cursor()

        # Create cars_country table
        cur.execute("DROP TABLE IF EXISTS cars_country")
        cur.execute("CREATE TABLE cars_country(id INTEGER NOT NULL PRIMARY KEY, name VARCHAR(30) NOT NULL, image VARCHAR(30) NOT NULL);")

        # Create cars_make, cars_model and cars_car tables
        cur.execute("DROP TABLE IF EXISTS cars_make")
        cur.execute("DROP TABLE IF EXISTS cars_model")
        cur.execute("DROP TABLE IF EXISTS cars_car")
    
        cur.execute("CREATE TABLE cars_make(id INTEGER NOT NULL PRIMARY KEY, name VARCHAR(30) NOT NULL);")
        cur.execute("CREATE TABLE cars_model(id INTEGER NOT NULL PRIMARY KEY, name VARCHAR(30) NOT NULL);")
        cur.executescript("""
        CREATE TABLE "cars_car" (
        "id" integer NOT NULL PRIMARY KEY,
        "year" integer NOT NULL,
        "make_id" integer NOT NULL REFERENCES "cars_make" ("id"),
        "model_id" integer NOT NULL REFERENCES "cars_model" ("id"),
        "country_id" integer NOT NULL REFERENCES "cars_country" ("id"),
        "market_country_id" integer NOT NULL REFERENCES "cars_country" ("id"),
        "msrp" integer NOT NULL,
        "num_doors" integer NOT NULL,
        "horsepower" integer NOT NULL,
        "horsepower_rpm" varchar(30) NOT NULL,
        "torque" integer NOT NULL,
        "torque_rpm" varchar(30) NOT NULL,
        "cylinders" integer NOT NULL,
        "engine_config" varchar(1) NOT NULL,
        "engine_displacement" real NOT NULL,
        "engine_special" varchar(13) NOT NULL,
        "trim" varchar(100) NOT NULL,
        "mpg_city" integer NOT NULL,
        "mpg_highway" integer NOT NULL,
        "mpg_combined" integer NOT NULL,
        "pass_capacity" integer NOT NULL,
        "pass_vol" real NOT NULL,
        "cargo_vol" real NOT NULL,
        "transmission" varchar(10) NOT NULL,
        "trans_gears" integer NOT NULL,
        "curb_weight" real NOT NULL,
        "drivetrain" varchar(2) NOT NULL,
        "body_style" varchar(30) NOT NULL
        );
        """)

        # Insert car data into tables
        for car in json_data:
        
            make_id = 0
            model_id = 0
            country_id = 0
            market_country_id = 0

            # check if countries already exists
            cur.execute("SELECT id FROM cars_country WHERE name=\"%s\"" % car["country"])
            row = cur.fetchone()
            if row == None:
                cur.execute("INSERT INTO cars_country(name, image) VALUES('%s', '%s');" % (car["country"], country_image_filename(car["country"])))
                country_id = cur.lastrowid
            else:
                country_id = row[0]

            cur.execute("SELECT id FROM cars_country WHERE name=\"%s\"" % car["market_country"])
            row = cur.fetchone()
            if row == None:
                cur.execute("INSERT INTO cars_country(name, image) VALUES('%s', '%s');" % (car["market_country"], country_image_filename(car["market_country"])))
                market_country_id = cur.lastrowid
            else:
                market_country_id = row[0]
        
            # check if make already exists
            cur.execute("SELECT id FROM cars_make WHERE name=\"%s\"" % car["make"])
            row = cur.fetchone()
            if row == None:
                cur.execute("INSERT INTO cars_make(name) VALUES('%s');" % car["make"])
                make_id = cur.lastrowid
            else:
                make_id = row[0]
        
            # check if model already exists
            cur.execute("SELECT id FROM cars_model WHERE name=\"%s\"" % car["model"])
            row = cur.fetchone()
            if row == None:
                cur.execute("INSERT INTO cars_model(name) VALUES('%s');" % car["model"])
                model_id = cur.lastrowid
            else:
                model_id = row[0]

            # insert the car
            print repr(car)
            cur.execute("INSERT INTO cars_car(year, make_id, model_id, country_id, market_country_id, msrp, num_doors, horsepower, horsepower_rpm, torque, torque_rpm, cylinders, engine_config, engine_displacement, engine_special, trim, mpg_city, mpg_highway, mpg_combined, pass_capacity, pass_vol, cargo_vol, transmission, trans_gears, curb_weight, drivetrain, body_style) VALUES(%d, %d, %d, %d, %d, %d, %d, %d, '%s', %d, '%s', %d, '%s', %.1f, '%s', '%s', %d, %d, %d, %d, %.1f, %.1f, '%s', %d, %.1f, '%s', '%s');" % (car["year"], make_id, model_id, country_id, market_country_id, car["msrp"], car["num_doors"], car["horsepower"], car["horsepower_rpm"], car["torque"], car["torque_rpm"], car["cylinders"], car["engine_config"], car["engine_displacement"], car["engine_special"], car["trim"], car["mpg_city"], car["mpg_highway"], car["mpg_combined"], car["pass_capacity"], car["pass_vol"], car["cargo_vol"], car["transmission"], car["trans_gears"], car["curb_weight"], car["drivetrain"], car["body_style"]))

        print "finished inserting cars into db"
        cur.execute("SELECT count(*) FROM cars_car")
        row = cur.fetchone()
        if row:
            print"Added %d cars" % row[0]

if __name__ == "__main__":
    db_populate()
