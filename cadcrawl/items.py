from scrapy.item import Item, Field

class CarItem(Item):
    year = Field() # 2009
    make = Field() # Infiniti
    model = Field() # G37
    country = Field() # Japan
    market_country = Field() # USA
    msrp = Field() # $36765
    num_doors = Field() # 2
    horsepower = Field() # 330
    horsepower_rpm = Field() # @ 7000 RPM
    torque = Field() # 270
    torque_rpm = Field() # @ 5200 RPM
    cylinders = Field() # V6
    engine_config = Field() # V/I/W/B
    engine_displacement = Field() # 3.7 L
    engine_special = Field() # Supercharged/Turbo/Hybrid
    trim = Field() # Journey -- Type S, Touring, etc.
    mpg_city = Field() # 18 mpg
    mpg_highway = Field() # 21 mpg
    mpg_combined = Field() # 22 mpg
    pass_capacity = Field() # 5
    pass_vol = Field() # 85.0 cu ft
    cargo_vol = Field() # 7.4 cu ft
    transmission = Field() # Automatic, manual, CVT
    trans_gears = Field() # 6 speed auto, etc.
    curb_weight = Field() # 3633 lbs
    drivetrain = Field() # FR, FF, FX, etc.
    body_style = Field() # Coupe

