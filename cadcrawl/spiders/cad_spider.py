from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from cadcrawl.items import CarItem
import re
import logging

logging.basicConfig(filename="cadcrawl.out", filemode="w", level=logging.DEBUG)

class CadSpider(BaseSpider):
    name = "cad"
    allowed_domains = ["buyersguide.caranddriver.com"]
    
    def gen_urls():
        urls = []
        with open('spec_urls.txt', 'r') as f:
            urls = [line.strip() for line in f.readlines()]
        return urls
    
    start_urls = gen_urls()
    #rules = [Rule(SgmlLinkExtractor(allow=['/specs/\d{6}']), 'parse_carspecs')]
     
    def parse(self, response):
        """
        parses caranddriver.com buyersguide specs page
        """
        x = HtmlXPathSelector(response)

        car = CarItem()

        car['year'] = response.url.split('/')[5]
        if car.get('year', '') == "":
            # thanks mini clubman, we'll get the year anyhow
            car['year'] = x.select('//div[@class="primary"]/h1/strong/text()').extract()[0].split()[0]

        make = x.select('//div[@class="content"]/p[@id="make"]/a/span/text()').extract()[0]
        car['make'] = make
        model = x.select('//div[@class="content"]/p[@id="model"]/a/span/text()').extract()[0]
        car['model'] = model

        car['country'] = "USA"
        car['market_country'] = "USA"

        msrp_trim = x.select('//div/table[@class="summary-table"]/tbody/tr/td/div[@id="specs_section_details_table"]/p/strong/text()').extract()[0].split()
        car['msrp'] = msrp_trim[0]
        # skip past msrp and model name to get the rest of the words making up the trim
        trim = msrp_trim[len(car['model'].split())+1:]
        car['trim'] = ' '.join(trim)

        doors = x.select('//div/table[@class="summary-table"]/tbody/tr/td/div[@id="specs_section_details_table"]/p[2]/text()').extract()[0].split()[0].split('-')[0]
        car['num_doors'] = doors

        spec_rows = x.select('//div[@id="tab-specifications"]/table[@class="summary-table"]/tbody[@class="corresponding-rows-on"]/tr')

        for row in spec_rows:
            data = row.select('td/text()').extract()
            td = data[0].lower()
            if td == "sae net horsepower @ rpm":
                hp_rpm = data[1].split()
                car['horsepower'] = hp_rpm[0]
                try:
                    # special cases have 2 hp's listed seperated by a '/', thus the split
                    car['horsepower_rpm'] = hp_rpm[2].split('/')[0]
                except IndexError:
                    # not all cars have the @ RPM value
                    car['horsepower_rpm'] = ''
            elif td == "sae net torque @ rpm":
                torque_rpm = data[1].split()
                car['torque'] = torque_rpm[0]
                try:
                    # special cases have 2 torque's listed seperated by a '/', thus the split
                    car['torque_rpm'] = torque_rpm[2].split('/')[0]
                except IndexError:
                    # not all cars have the @ RPM value
                    car['torque_rpm'] = ''
            elif td == "engine type":
                eng_type = re.search('(V|V-|I|W|H|Flat|Gas)\s*(\d+)', data[1])
                if not eng_type:
                    # likely an electric car
                    cyl = 0
                    car['engine_config'] = ""
                else:
                    eng_conf = eng_type.group(1).split('-')[0] # fuck you V-
                    cyl = eng_type.group(2)
                    car['engine_config'] = "Flat" if (eng_conf == "Gas") else eng_conf
                car['cylinders'] = cyl
                eng_special = re.search('(turbo|turbocharged|supercharged|hybrid)', data[1].lower())
                car['engine_special'] = "" if not eng_special else eng_special.group()
            elif td == "displacement":
                displacement = re.search('(\d\.\d)', data[1])
                car['engine_displacement'] = displacement.group()
            elif td == "epa fuel economy est - hwy" or td == "epa mpg equivalent - hwy":
                # don't reset the value if set before - certain pages have #s of mpgs
                if car.get('mpg_highway', 0) == 0:
                    car['mpg_highway'] = data[1].split()[0]
            elif td == "epa fuel economy est - city" or td == "epa mpg equivalent - city":
                # don't reset the value if set before - certain pages have #s of mpgs
                if car.get('mpg_city', 0) == 0:
                    car['mpg_city'] = data[1].split()[0]
            elif td == "fuel economy est-combined" or td == "epa mpg equivalent - combined":
                # don't reset the value if set before - certain pages have #s of mpgs
                if car.get('mpg_combined', 0) == 0:
                    car['mpg_combined'] = data[1].split()[0]
            elif td == "passenger capacity":
                car['pass_capacity'] = data[1].split()[0]
            elif td == "passenger volume":
                car['pass_vol'] = data[1].split()[0]
            elif td == "trunk volume":
                car['cargo_vol'] = data[1].split()[0][0:2]
            elif td == "trans type":
                car['trans_gears'] = data[1].strip()
                try:
                    float(car['trans_gears'])
                except ValueError:
                    car['trans_gears'] = u'0'
            elif td == "base curb weight" or td == "curb weight":
                # don't reset the value if set before - certain pages have #s of curb weights
                if car.get('curb_weight', 0) == 0:
                    car['curb_weight'] = data[1].split()[0]
            elif td == "drivetrain":
                car['drivetrain'] = data[1].strip()
            elif td == "trans description cont.":
                transmission = data[1].strip()
                if transmission.lower() == "continuously variable ratio" or transmission.lower() == "continuously variable":
                    car['transmission'] = "CVT"
                else:
                    car['transmission'] = transmission.split()[0]
        
        body_style_search = x.select('//div[@id="leaderboard-ad"]/script/text()').extract()[0]
        body_style = re.search("bodyStyle\s+:\s+'([A-Za-z\s]+)'", body_style_search)
        if body_style:
            car['body_style'] = body_style.group(1)
        else:
            car['body_style'] = "N/A"
        return car

