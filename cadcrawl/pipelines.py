import logging
import scrapy.exceptions

logging.getLogger("cadcrawl.out")

class CadValidatePipeline(object):
    
    def process_item(self, item, spider):
        """
        validate data from caranddriver.com specs page scrape
        """
        self.check_string('body_style', item)
        self.check_float('cargo_vol', item)

        item.get('country', 'USA')
        self.set_country(item)
        item.get('market_country', 'USA')

        item.get('curb_weight', 'N/A')
        if ',' in item['curb_weight']:
            # convert 2,302 to 2302
            item['curb_weight'] = item['curb_weight'].replace(',', '')
        if '-' in item['curb_weight']:
            # if range, 1200-1400, take 1200
            item['curb_weight'] = item['curb_weight'].split('-')[0]
        self.check_float('curb_weight', item)

        self.check_int('cylinders', item)

        self.check_string('drivetrain', item)
        if item['drivetrain'].lower() == "rear wheel drive" or "rear-wheel drive":
            item['drivetrain'] = "FR"
        elif item['drivetrain'].lower() == "front wheel drive" or "front-wheel drive":
            item['drivetrain'] = "FF"
        elif item['drivetrain'].lower() == "all wheel drive" or "4 wheel drive" or "all-wheel drive":
            item['drivetrain'] = "FA"

        self.check_string('engine_config', item)
        self.check_float('engine_displacement', item)
        self.check_string('engine_special', item)
        self.check_int('horsepower', item)
        self.check_string('horsepower_rpm', item)
        self.check_string('make', item)
        self.check_string('model', item)
        self.check_int('mpg_city', item)
        self.check_int('mpg_highway', item)

        if item.get('mpg_combined', 'N/A') == "N/A":
            item['mpg_combined'] = '%d' % ((item['mpg_city'] + item['mpg_highway']) / 2)
        if '/' in item['mpg_combined']:
            # if mpg_combined is 15/22, grab the 22
            item['mpg_combined'] = item['mpg_combined'].split('/')[1]
        self.check_int('mpg_combined', item)

        try:
            item['msrp'] = int(item['msrp'][1:])
        except ValueError, KeyError:
            item['msrp'] = 0

        self.check_int('num_doors', item)
        self.check_int('pass_capacity', item)
        self.check_float('pass_vol', item)
        self.check_int('torque', item)
        self.check_string('torque_rpm', item)
        self.check_int('trans_gears', item)
        self.check_string('transmission', item)
        if ',' in item['transmission']:
            item['transmission'] = item['transmission'].replace(',', '')
        self.check_string('trim', item)
        if "'" in item['trim']:
            # single quotes can throw sql queries off - replace with 2 single quotes
            item['trim'] = item['trim'].replace("'", "''")
        self.check_int('year', item)

        return item

    def check_int(self, key, d):
        """
        Check that key is a int value in dictionary d.
        Insert 0 if key doesn't exist in d or error.
        """
        if key not in d or d[key] == "N/A":
            d[key] = 0
        try:
            d[key] = int(d[key])
        except ValueError:
            try:
                d[key] = int(float(d[key]))
            except ValueError:
                d[key] = 0

    def check_float(self, key, d):
        """
        Check that key is a float value in dictionary d.
        Insert 0.0 if key doesn't exist in d or error.
        """
        if key not in d or d[key] == "N/A":
            d[key] = 0.0
        try:
            d[key] = float(d[key])
        except ValueError:
            d[key] = 0.0

    def check_string(self, key, d):
        """
        Check that key is a string value in dictionary d.
        Insert '' empty string if key doesn't exist in d or error.
        """
        if key not in d or d[key] == "N/A" or d[key] == "N/" or d[key] == "-TBD-":
            d[key] = ''

    def set_country(self, car):
        """
        Set car's country value based on its make.
        """
        if car['make'] == "Bugatti":        ## France
            car['country'] = "France"
        elif car['make'] == "Audi":         ## Germany
            car['country'] = "Germany"
        elif car['make'] == "BMW":
            car['country'] = "Germany"
        elif car['make'] == "Maybach":
            car['country'] = "Germany"
        elif car['make'] == "Mercedes-Benz":
            car['country'] = "Germany"
        elif car['make'] == "Porsche":
            car['country'] = "Germany"
        elif car['make'] == "Smart":
            car['country'] = "Germany"
        elif car['make'] == "Volkswagen":
            car['country'] = "Germany"
        elif car['make'] == "Alfa Romeo":   ## Italy
            car['country'] = "Italy"
        elif car['make'] == "Ferrari":
            car['country'] = "Italy"
        elif car['make'] == "Fiat":
            car['country'] = "Italy"
        elif car['make'] == "Lamborghini":
            car['country'] = "Italy"
        elif car['make'] == "Maserati":
            car['country'] = "Italy"
        elif car['make'] == "Pagani":
            car['country'] = "Italy"
        elif car['make'] == "Acura":        ## Japan
            car['country'] = "Japan"
        elif car['make'] == "Honda":
            car['country'] = "Japan"
        elif car['make'] == "Infiniti":
            car['country'] = "Japan"
        elif car['make'] == "Isuzu":
            car['country'] = "Japan"
        elif car['make'] == "Lexus":
            car['country'] = "Japan"
        elif car['make'] == "Mazda":
            car['country'] = "Japan"
        elif car['make'] == "Mitsubishi":
            car['country'] = "Japan"
        elif car['make'] == "Nissan":
            car['country'] = "Japan"
        elif car['make'] == "Scion":
            car['country'] = "Japan"
        elif car['make'] == "Subaru":
            car['country'] = "Japan"
        elif car['make'] == "Suzuki":
            car['country'] = "Japan"
        elif car['make'] == "Toyota":
            car['country'] = "Japan"
        elif car['make'] == "Spyker":       ## Netherlands
            car['country'] = "Netherlands"
        elif car['make'] == "Kia":          ## South Korea
            car['country'] = "South Korea"
        elif car['make'] == "Volvo":        ## Sweden
            car['country'] = "Sweden"
        elif car['make'] == "Aston Martin": ## UK
            car['country'] = "UK"
        elif car['make'] == "Bentley":
            car['country'] = "UK"
        elif car['make'] == "Jaguar":
            car['country'] = "UK"
        elif car['make'] == "Land Rover":
            car['country'] = "UK"
        elif car['make'] == "Lotus":
            car['country'] = "UK"
        elif car['make'] == "McLaren":
            car['country'] = "UK"
        elif car['make'] == "Mini":
            car['country'] = "UK"
        elif car['make'] == "Morgan":
            car['country'] = "UK"
        elif car['make'] == "Rolls-Royce":
            car['country'] = "UK"

class CadDuplicatesPipeline(object):

    def __init__(self):
        self.cars_seen = set()
    
    def process_item(self, item, spider):
        """
        check for duplicate car scrapes
        """
        car_id = " ".join([str(item['year']), item['make'], item['model'], item['trim']])
        if car_id in self.cars_seen:
            logging.debug("Oops! Duplicate car: %s" % car_id)
            raise scrapy.exceptions.DropItem("Duplicate item found: %s" % item)
        else:
            self.cars_seen.add(car_id)
            logging.debug("Added car: %s" % car_id)
            return item

