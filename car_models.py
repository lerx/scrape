#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Module to define car data models that represent a vehicle.
"""

import json


class Group(object):
    """Group model data"""
    def __init__(self):
        self.name = str()
        self.founded_year = int(0)

    def __str__(self):
        group_info = ("Group: {}".format(self.name),
                      "Founded: {}".format(self.founded_year))
        return '\n'.join(group_info)


class Model(object):
    """Vehicle Model data"""
    def __init__(self):
        self.name = str()
        self.intro_year = int(0)
        self.exit_year = int(0)

    def __str__(self):
        model_info = (self.name,
                      "Intro Year: {}".format(self.intro_year),
                      "Exit Year: {}".format(self.exit_year))
        return '\n'.join(model_info)


class Make(object):
    """Make model data"""
    def __init__(self):
        self.name = str()
        self.founded = None  # DateTime
        self.headquarters = None
        #parent = models.ForeignKey('self', related_name='+', null=True)
        self.owner = None #Group()
        self.intro_year = int(0)
        self.exit_year = int(0)

    def __str__(self):
        make_info = (self.name,
                     "Founded: {}".format(self.founded),
                     "HQ: {}".format(self.headquarters),
                     "Owner: {}".format(self.owner),
                     "Intro Year: {}".format(self.intro_year),
                     "Exit Year: {}".format(self.exit_year))
        return '\n'.join(make_info)


class Trim(object):
    """Trim data model."""
    def __init__(self):
        self.name = str()

    def __str__(self):
        return self.name


class Country(object):
    """County data model."""
    def __init__(self):
        self.name = str()
        self.code_iso = str()
        self.code_un = str()


class ExteriorColor(object):
    """Exterior color data model."""
    def __init__(self):
        self.name = str()
        self.code = str()

    def __str__(self):
        return "Name: {} Code: {}".format(self.name, self.code)


class InteriorColor(object):
    """Interior color data model."""
    def __init__(self):
        self.name = str()
        self.material = str()

    def __str__(self):
        return "Name: {} Material: {}".format(self.name, self.material)


class Engine(object):
    """Engine model data"""
    def __init__(self):
        self.code = str()
        self.cylinders = int(0)
        self.config = str()
        self.forced_induction = None #str()
        self.engine_type = str()
        self.displacement_l = float(0.0)
        self.displacement_cc = float(0.0) # TODO
        self.displacement_cu_in = float(0.0)
        self.bore_mm = float(0.0)
        self.bore_in = float(0.0)
        self.stroke_mm = float(0.0)
        self.stroke_in = float(0.0)
        self.compression_ratio = None #str()
        self.valves = int(0)
        self.valves_per_cylinder = int(0)
        self.max_power_hp = int(0)
        self.max_power_ps = int(0) # TODO
        self.max_power_kw = int(0) # TODO
        self.max_power_rpm = int(0)
        self.max_torque_nm = int(0) # TODO
        self.max_torque_lb_ft = int(0)
        self.max_torque_kgf_m = int(0) # TODO
        self.max_torque_rpm = int(0)
        self.red_line = int(0)
        self.fuel_type = str()
        self.fuel_delivery = str()

    def __str__(self):
        engine_info = ("Code: {}".format(self.code),
                       "Cylinders: {}".format(self.cylinders),
                       "Config: {}".format(self.config),
                       "Engine Type: {}".format(self.engine_type),
                       "Displacement: {}L".format(self.displacement_l),
                       "Displacement: {} cu in".format(self.displacement_cu_in),
                       "Horsepower: {} @ {} rpm".format(self.max_power_hp, self.max_power_rpm),
                       "Torque: {} @ {} rpm".format(self.max_torque_lb_ft, self.max_torque_rpm),
                       "Fuel Type: {}".format(self.fuel_type),
                       "Fuel Delivery: {}".format(self.fuel_delivery))
        return '\n'.join(engine_info)


class Transmission(object):
    """Transmission model data."""
    def __init__(self):
        self.code = str()
        self.transmission_type = str()
        self.number_of_gears = int(0)

    def __str__(self):
        trans_info = ("Transmission Code: {}".format(self.code),
                      "Transmission Type: {}".format(self.transmission_type),
                      "Number of Gears: {}".format(self.number_of_gears))
        return '\n'.join(trans_info)


class Drivetrain(object):
    """Drivetrain model data."""
    def __init__(self):
        self.engine = Engine()
        self.transmission = Transmission()
        self.drive = str()

    def __str__(self):
        drivetrain_info = ("Engine: {}".format(self.engine),
                           "Transmission: {}".format(self.transmission),
                           "Drive: {}".format(self.drive))
        return '\n'.join(drivetrain_info)


class BodyStyle(object):
    """Vehicle Body Style data model."""
    def __init__(self):
        self.name = str()

    def __str__(self):
        return self.name


class Classification(object):
    """Vehicle Classification data model."""
    def __init__(self):
        self.name = str()

    def __str__(self):
        return self.name


class Configuration(object):
    """Vehicle configuration data model."""
    def __init__(self):
        self.drivetrain = Drivetrain()
        self.name = str()
        self.mpg_city = int(0)
        self.mpg_highway = int(0)
        self.mpg_combined = float(0.0)
        self.seats = int(0)
        self.msrp = int(0)
        self.number_of_doors = int(0)
        self.passenger_vol_cu_in = float(0.0)
        self.cargo_vol_cu_in = float(0.0) # TODO
        self.curb_weight_kg = float(0.0) # TODO
        self.curb_weight_lb = float(0.0)
        self.length_mm = int(0) # TODO
        self.length_in = float(0.0)
        self.width_mm = int(0) # TODO
        self.width_in = float(0.0)
        self.height_mm = int(0) # TODO
        self.height_in = float(0.0)
        self.wheelbase_mm = int(0)
        self.wheelbase_in = float(0.0)
        self.fuel_capacity_l = float(0.0) # TODO
        self.fuel_capacity_g = float(0.0)
        self.platform = None #str()
        self.body_style = BodyStyle()
        self.classification = Classification()

    def __str__(self):
        conf = ("Name: {}".format(self.name),
                "Drivetrain: {}".format(self.drivetrain),
                "MPG city: {}".format(self.mpg_city),
                "MPG highway: {}".format(self.mpg_highway),
                "MPG combined: {}".format(self.mpg_combined),
                "Seats: {}".format(self.seats),
                "MSRP: ${}".format(self.msrp),
                "Doors: {}".format(self.number_of_doors),
                "Passenger Vol: {} cubic in".format(self.passenger_vol_cu_in),
                "Curb Weight: {} lbs".format(self.curb_weight_lb),
                "Length: {} in".format(self.length_in),
                "Width: {} in".format(self.width_in),
                "Height: {} in".format(self.height_in),
                "Wheelbase: {} in".format(self.wheelbase_in),
                "Fuel Capacity: {} gal".format(self.fuel_capacity_g),
                "Platform: {}".format(self.platform),
                "Body Style: {}".format(self.body_style),
                "Classification: {}".format(self.classification))
        return '\n'.join(conf)


class AvailableExteriorColor(object):
    def __init__(self):
        self.color = ExteriorColor()
        self.trim = Trim()


class AvailableInteriorColor(object):
    def __init__(self):
        self.color = InteriorColor()
        self.trim = Trim()


class Vehicle(object):
    """Vehicle model data"""
    def __init__(self):
        self.year = int()
        self.make = Make()
        self.model = Model()
        self.trim = Trim()
        self.configuration = Configuration()
        self.generation = None
        self.origin_country = Country()
        self.market_country = Country()
        #sisters = models.ForeignKey('self', related_name='+', null=True)
        #other_names = models.ForeignKey('self', related_name='+', null=True)
        self.available_interior_color = AvailableInteriorColor()
        self.available_exterior_color = AvailableExteriorColor()

    def __str__(self):
        vehicle_str = ("=============================================",
                       "Year: {}".format(self.year),
                       "Make: {}".format(self.make),
                       "Model: {}".format(self.model),
                       "Trim: {}".format(self.trim),
                       "Config: {}".format(self.configuration),
                       "=============================================")
        return '\n'.join(vehicle_str)

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=False, indent=4)
